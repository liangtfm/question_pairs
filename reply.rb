require 'sqlite3'

class Reply

  def self.all
    replies = QuestionDatabase.instance.execute("SELECT * FROM replies")
    replies.map { |reply| Reply.new(reply) }
  end

  attr_reader :id, :question_id, :parent_reply_id, :user_id, :body

  def initialize(options = {})
    @id, @question_id, @parent_reply_id, @user_id, @body =
     options.values_at("id", "question_id", "parent_reply_id", "user_id", "body")
  end

  def create
    raise "already saved!" unless self.id.nil?

    params = [self.question_id, self.parent_reply_id, self.user_id, self.body]
    QuestionDatabase.instance.execute(<<-SQL, *params)
      INSERT INTO
        replies (question_id, parent_reply_id, user_id, body)
      VALUES
        (?, ?, ?, ?)
    SQL
  end

  def self.find_by_id(reply_id)
    reply = QuestionDatabase.instance.execute(<<-SQL, reply_id)
        SELECT *
        FROM replies
        WHERE replies.id = ?
      SQL

    raise "no reply found" if reply.empty?
    Reply.new(reply.first)
  end

  def self.find_by_question_id(question_id)
    replies = QuestionDatabase.instance.execute(<<-SQL, question_id)
    SELECT *
    FROM replies
    WHERE replies.question_id = ?
    SQL

    replies.map { |reply| Reply.new(reply) }
  end

  def self.find_by_user_id(user_id)
    replies = QuestionDatabase.instance.execute(<<-SQL, user_id)
        SELECT *
        FROM replies
        WHERE replies.user_id = ?
      SQL

    replies.map { |reply| Reply.new(reply) }
  end

  def author
    User.find_by_id(self.user_id)
  end

  def question
    Question.find_by_author_id(self.user_id)
  end

  def parent_reply
    Reply.find_by_id(self.parent_reply_id)
  end

  def child_replies
    children = QuestionDatabase.instance.execute(<<-SQL, self.id)
        SELECT *
        FROM replies
        WHERE replies.parent_reply_id = ?
      SQL

    children.map { |child| Reply.new(child) }
  end

  def save
    self.id.nil? ? create : update
  end

  def update
    raise "reply doesn't exist" if self.id.nil?

    params = [self.question_id, self.parent_reply_id, self.user_id, self.body]
    QuestionDatabase.instance.execute(<<-SQL, *params, self.id)
      UPDATE replies
      SET (question_id, parent_reply_id, user_id, body) = (?, ?, ?, ?)
      WHERE replies.id = ?
    SQL
  end
end