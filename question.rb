require 'sqlite3'

class Question

  def self.all
    questions = QuestionDatabase.instance.execute("SELECT * FROM questions")
    questions.map { |question| Question.new(question) }
  end

  attr_reader :id, :title, :question, :author_id

  def initialize(options = {})
    @id, @title, @question, @author_id =
      options.values_at("id", "title", "question", "author_id")
  end

  def create
    raise "already saved!" unless self.id.nil?

    params = [self.title, self.question, self.author_id]
    QuestionDatabase.instance.execute(<<-SQL, *params)
      INSERT INTO
        questions (title, question, author_id)
      VALUES
        (?, ?, ?)
    SQL

    @id = QuestionDatabase.instance.last_insert_row_id
  end

  def self.find_by_id(question_id)
    question = QuestionDatabase.instance.execute(<<-SQL, question_id)
        SELECT *
        FROM questions
        WHERE questions.id = ?
      SQL

    raise "question not found" if question.empty?
    Question.new(question.first)
  end

  def self.find_by_author_id(author_id)
    questions = QuestionDatabase.instance.execute(<<-SQL, author_id)
        SELECT *
        FROM questions
        WHERE questions.author_id = ?
      SQL

    questions.map { |question| Question.new(question) }
  end

  def author
    User.find_by_id(self.author_id)
  end

  def replies
    Reply.find_by_question_id(self.id)
  end

  def followers
    QuestionFollower.followers_for_question_id(self.id)
  end

  def self.most_followed(n)
    QuestionFollower.most_followed_questions(n)
  end

  def likers
    QuestionLike.likers_for_question_id(self.id)
  end

  def num_likes
    QuestionLike.num_likes_for_question_id(self.id)
  end

  def self.most_liked(n)
    QuestionLike.most_liked_questions(n)
  end

  def save
    self.id.nil? ? create : update
  end

  def update
    raise "question doesn't exist" if self.id.nil?

    params = [self.title, self.question, self.author_id]
    QuestionDatabase.instance.execute(<<-SQL, *params, self.id)
      UPDATE questions
      SET (title, question, author_id) = (?, ?, ?)
      WHERE questions.id = ?
    SQL
  end
end