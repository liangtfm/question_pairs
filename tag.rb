require 'sqlite3'

class Tag

  def self.all
    tags = QuestionDatabase.instance.execute("SELECT * FROM tags")
    tags.map { |tag| Tag.new(tag) }
  end

  attr_reader :id, :name

  def initialize(options = {})
    @id, @name = options.values_at("id", "name")
  end

  def most_popular_questions(n)
    questions = QuestionDatabase.instance.execute(<<-SQL, self.id, n)
    SELECT q.id, q.title, q.question, q.author_id
    FROM question_tags qt JOIN questions q ON qt.question_id = q.id
    WHERE qt.tag_id = ?
    ORDER BY (SELECT COUNT(ql.user_id)
              FROM question_likes ql
              WHERE ql.question_id = qt.question_id) DESC
    LIMIT ?
    SQL

    questions.map { |question| Question.new(question) }
  end

  def self.most_popular(n)
    most_popular_tags = QuestionDatabase.instance.execute(<<-SQL, n)
      SELECT tags.id, tags.name
      FROM tags
        JOIN question_tags qt ON tags.id = qt.tag_id
        JOIN question_likes ql ON ql.question_id = qt.question_id
      GROUP BY qt.tag_id
      ORDER BY COUNT(ql.user_id) DESC
      LIMIT ?
    SQL

    most_popular_tags.map { |tag| Tag.new(tag) }
  end
end

