require 'sqlite3'

class QuestionLike

  # def self.all
  #   likes = QuestionDatabase.instance.execute("SELECT * FROM question_likes")
  #   likes.map { |like| QuestionLike.new(like) }
  # end

  attr_reader :id, :user_id, :question_id

  # def initalize(options = {})
  #   @id, @user_id, @question_id = options.values_at("def", "user_id", "question_id")
  # end

  # def create
  #   raise "already saved!" unless self.id.nil?
  #
  #   params = [self.user_id, self.question_id]
  #   QuestionDatabase.instance.execute(<<-SQL, *params)
  #     INSERT INTO
  #       question_likes (user_id, question_id)
  #     VALUES
  #       (?, ?)
  #   SQL
  # end

  def self.find_by_id(question_like_id)
    like = QuestionDatabase.instance.execute(<<-SQL, question_like_id)
        SELECT *
        FROM question_likes
        WHERE question_likes.id = ?
      SQL

    raise "no record found" if like.empty?
    QuestionLike.new(like.first)
  end

  def self.likers_for_question_id(question_id)
    likers = QuestionDatabase.instance.execute(<<-SQL, question_id)
        SELECT users.id, users.fname, users.lname
        FROM question_likes ql JOIN users ON ql.user_id = users.id
        WHERE ql.question_id = ?
      SQL

    likers.map { |liker| User.new(liker) }
  end

  def self.num_likes_for_question_id(question_id)
    num_likes = QuestionDatabase.instance.execute(<<-SQL, question_id)
        SELECT COUNT(ql.user_id) AS count
        FROM question_likes ql
        WHERE ql.question_id = ?
      SQL

    num_likes.first["count"]
  end

  def self.liked_questions_for_user_id(user_id)
    liked_questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
        SELECT q.id, q.title, q.question, q.author_id
        FROM question_likes ql JOIN questions q ON ql.question_id = q.id
        WHERE ql.user_id = ?
      SQL

    liked_questions.map { |question| Question.new(question) }
  end

  def self.most_liked_questions(n)
    questions = QuestionDatabase.instance.execute(<<-SQL, n)
        SELECT q.id, q.title, q.question, q.author_id
        FROM question_likes ql JOIN questions q ON ql.question_id = q.id
        GROUP BY ql.question_id
        ORDER BY COUNT(ql.user_id) DESC
        LIMIT ?
      SQL

    questions.map { |question| Question.new(question) }
  end
end