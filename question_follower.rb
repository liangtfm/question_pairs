require 'sqlite3'

class QuestionFollower

  # def self.all
  #   followers = QuestionDatabase.instance.execute("SELECT * FROM question_followers")
  #   followers.map { |follower| QuestionFollower.new(follower) }
  # end

  attr_reader :id, :user_id, :question_id

  # def initialize(options = {})
  #   @id, @user_id, @question_id =
  #     options.values_at("id", "user_id", "question_id")
  # end

  # def create
  #   raise "already saved!" unless self.id.nil?
  #
  #   params = [self.user_id, self.question_id]
  #   QuestionDatabase.instance.execute(<<-SQL, *params)
  #     INSERT INTO
  #       question_followers (user_id, question_id)
  #     VALUES
  #       (?, ?)
  #   SQL
  #
  #   @id = QuestionDatabase.instance.last_insert_row_id
  # end

  # def self.find_by_id(question_follower_id)
  #   follower = QuestionDatabase.instance.execute(<<-SQL, question_follower_id)
  #       SELECT *
  #       FROM question_followers
  #       WHERE question_followers.id = ?
  #     SQL
  #
  #   raise "no record found" if follower.empty?
  #   QuestionFollower.new(follower.first)
  # end

  def self.followers_for_question_id(question_id)
    followers = QuestionDatabase.instance.execute(<<-SQL, question_id)
        SELECT users.id, users.fname, users.lname
        FROM question_followers JOIN users ON users.id = question_followers.user_id
        WHERE question_followers.question_id = ?
      SQL

    followers.map { |follower| User.new(follower) }
  end

  def self.followed_questions_for_user_id(user_id)
    questions = QuestionDatabase.instance.execute(<<-SQL, user_id)
        SELECT q.id, q.title, q.question, q.author_id
        FROM question_followers qf JOIN questions q ON qf.question_id = q.id
        WHERE qf.user_id = ?
      SQL

    questions.map { |question| Question.new(question) }
  end

  def self.most_followed_questions(n)
    questions = QuestionDatabase.instance.execute(<<-SQL, n)
        SELECT q.id, q.title, q.question, q.author_id
        FROM question_followers qf JOIN questions q ON qf.question_id = q.id
        GROUP BY qf.question_id
        ORDER BY COUNT(qf.user_id) DESC
        LIMIT ?
      SQL

    questions.map { |question| Question.new(question) }
  end
end