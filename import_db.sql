CREATE TABLE users (
  id INTEGER PRIMARY KEY,
  fname VARCHAR(255) NOT NULL,
  lname VARCHAR(255) NOT NULL
);

CREATE TABLE questions (
  id INTEGER PRIMARY KEY,
  title VARCHAR(255) NOT NULL,
  question VARCHAR(255) NOT NULL,
  author_id INTEGER NOT NULL,

  FOREIGN KEY (author_id) REFERENCES users(id)
);

CREATE TABLE question_followers (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE replies (
  id INTEGER PRIMARY KEY,
  question_id INTEGER NOT NULL,
  parent_reply_id INTEGER,
  user_id INTEGER NOT NULL,
  body VARCHAR(255) NOT NULL,

  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (parent_reply_id) REFERENCES replies(id),
  FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE TABLE question_likes (
  id INTEGER PRIMARY KEY,
  user_id INTEGER NOT NULL,
  question_id INTEGER NOT NULL,

  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (question_id) REFERENCES questions(id)
);

CREATE TABLE tags (
  id INTEGER PRIMARY KEY,
  name VARCHAR(255)
);

CREATE TABLE question_tags (
  id INTEGER PRIMARY KEY,
  question_id INTEGER NOT NULL,
  tag_id INTEGER NOT NULL,

  FOREIGN KEY (question_id) REFERENCES questions(id),
  FOREIGN KEY (tag_id) REFERENCES tags(id)
);

INSERT INTO
  users (fname, lname)
VALUES
  ('Anthony', 'Liang'), ('Sett', 'Oo'), ('Sid', 'Ravel'), ('Tommy', 'Duek');

INSERT INTO
  questions (title, question, author_id)
VALUES
  ('how to indent mac os?', 'how do you indent in textmate?!!??!', 1),
  ('who is grumpycat?', 'really, who is he?', 2),
  ('two line tie breaker', 'for poker, how?!!?!', 3);

INSERT INTO
  question_followers(user_id, question_id)
VALUES
  (1,2), (2,3), (3,1);

INSERT INTO
  replies (question_id, parent_reply_id, user_id, body)
VALUES
  (1, NULL, 4, 'first!'), (1, 1, 3, 'second!'), (2, NULL, 1, 'cat!');

INSERT INTO
  question_likes (user_id, question_id)
VALUES
  (1,3), (2,3), (3,1), (4,2);

INSERT INTO
  tags (name)
VALUES
  ('html'), ('css'), ('ruby'), ('javascript'), ('mac'), ('memes');

INSERT INTO
  question_tags (question_id, tag_id)
VALUES
  (1,5), (2,6), (3,3), (3,4), (3,5);