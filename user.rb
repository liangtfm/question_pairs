require 'sqlite3'

class User
  def self.all
    users = QuestionDatabase.instance.execute("SELECT * FROM users")
    users.map { |user| User.new(user) }
  end

  attr_accessor :id, :fname, :lname

  def initialize(options = {})
    @id, @fname, @lname =
      options.values_at("id", "fname", "lname")
  end

  def create
    raise "already saved!" unless self.id.nil?

    params = [self.fname, self.lname]
    QuestionDatabase.instance.execute(<<-SQL, *params)
      INSERT INTO
        users (fname, lname)
      VALUES
        (?, ?)
    SQL

    @id = QuestionDatabase.instance.last_insert_row_id
  end

  def self.find_by_id(user_id)
    user = QuestionDatabase.instance.execute(<<-SQL, user_id)
        SELECT *
        FROM users
        WHERE users.id = ?
      SQL

    raise "user not found" if user.empty?
    User.new(user.first)
  end

  def self.find_by_name(fname, lname)
    users = QuestionDatabase.instance.execute(<<-SQL, fname, lname)
        SELECT *
        FROM users
        WHERE users.fname = ? AND users.lname = ?
      SQL

    raise "no user with given names found" if users.empty?
    users.map { |user| User.new(user) }
  end

  def authored_questions
    Question.find_by_author_id(self.id)
  end

  def authored_replies
    Reply.find_by_user_id(self.id)
  end

  def followed_questions
    QuestionFollower.followed_questions_for_user_id(self.id)
  end

  def liked_questions
    QuestionLike.liked_questions_for_user_id(self.id)
  end

  def average_karma
    average_karma = QuestionDatabase.instance.execute(<<-SQL, self.id)
        SELECT AVG(like_count) AS num_like
        FROM
          (SELECT COUNT(ql.user_id) AS like_count
           FROM question_likes ql JOIN questions q ON ql.question_id = q.id
           WHERE ql.question_id IN
             (SELECT q.id
              FROM questions q
              WHERE q.author_id = ?)
           GROUP BY ql.question_id)
       SQL

     average_karma.first["num_like"]
  end

  def save
    self.id.nil? ? create : update
  end

  def update
    raise "user doesn't exist" if self.id.nil?

    params = [self.fname, self.lname]
    QuestionDatabase.instance.execute(<<-SQL, *params, self.id)
      UPDATE users
      SET(fname, lname) = (?, ?)
      WHERE users.id = ?
    SQL
  end
end